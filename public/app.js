var app = angular.module('tcdash', [])

app.controller('MainController', function($scope, $http, $timeout) {
	loadBuilds()

	statuses = {
		SUCCESS: 'OK',
		FAILURE: 'FAILED',
	}
	
	var prevFailedCount = Number.MAX_VALUE;

	var voice = null
	speechSynthesis.getVoices()
	speechSynthesis.onvoiceschanged = function() {
		voice = speechSynthesis.getVoices().filter(x => x.name.toLowerCase().includes('zira'))[0]
	}

	function loadBuilds() {
		$http.get('/dash/api/builds').then(function(res) {
			const failedBuilds = res.data.filter(res => res.builds.build[0] && res.builds.build[0].status === 'FAILURE')

			$scope.builds = failedBuilds.map(function (b) {
				var build = b.builds.build[0];
				if (build.lastChanges) {
					var lastChanges = build.lastChanges.change[0].username
				}

				return {
					name: b.name + "::" + b.projectName,
					status: build.status,
					state: build.state,
					statusText: build.statusText,
					branch: build.branchName,
					lastChanges: lastChanges
				}
			})

			if ($scope.builds.length > 0) {
				$scope.state = 'builds';
			}
			else {
				// const date = new Date()
				// const time = date.getHours() * 100 + date.getMinutes();
				// if (time > 1029) {
				// 	if ($scope.state !== 'tsla') {
				// 		new Audio('./bell5.mp3').play().catch(error => {
				// 			speak('ding ding ding ding ding . . . Market is open')
				// 		})
				// 	}
				// 	$scope.state = 'tsla'
				// }
				// else {
					$scope.state = 'logs'
				// }
			}

			$scope.builds.forEach(x => {
				if (x.status !== undefined) {
				  x.status = x.state === 'running' ? 'running' : statuses[x.status] || x.status
				}
			});

			var failedCount = $scope.builds.filter(x => x.status === 'FAILED').length;
			if (failedCount > prevFailedCount) {
				// new Audio('./suffer.mp3').play()
				setTimeout(function() { announceBuild(failedBuilds[0]) }, 1000)
			}

			prevFailedCount = failedCount;

			var running = $scope.builds.some(x => x.state === 'running')
			$timeout(loadBuilds, (running ? 5 : 20) * 1000)

		}).catch(function(err) {
			console.error(err)
			$timeout(loadBuilds, 5 * 60 * 1000);
		})
	}

	function announceBuild(build) {
		var lastBuild = build.builds && build.builds && build.builds.build.length && build.builds.build[0] || {}
		var lastChange = lastBuild.lastChanges && lastBuild.lastChanges.change && lastBuild.lastChanges.change.length && lastBuild.lastChanges.change[0] || {}

		var text = 'ding ding. . . . . . ' + build.projectName + ' ' + build.name
			+ (lastBuild.branchName ? '. On branch ' + lastBuild.branchName : '')
			+ (lastBuild.status ? '. ' + lastBuild.status : '')
			+ (lastBuild.statusText ? '. ' + lastBuild.statusText : '')
			+ (lastChange.username ? '. By ' + lastChange.username : '')
			+ (lastChange.username ? '. ' + lastChange.comment : '')

		speak(text)
	}

	function speak(text) {
		console.log('speak ' + text)
		var utter = new SpeechSynthesisUtterance(text)
		utter.voice = voice
		utter.rate = 0.7
		window.speechSynthesis.speak(utter)

		var myImage = new Image();
		myImage.src = 'http://localhost:59347/speak?words=' + encodeURIComponent(text);
	}

})