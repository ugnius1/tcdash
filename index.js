const express = require('express')
const request = require('superagent')
const lessMiddleware = require('less-middleware')

const app = express()
const router = express.Router()

const config = require('./config.js')

router.use(lessMiddleware(__dirname + '/public'))
router.use(express.static(__dirname + '/public'))

router.get('/api/builds', (req, res, next) => {
	const authorization = 'Basic ' + new Buffer(config.user + ':' + config.password).toString('base64')
	const query = 'buildTypes?fields=buildType(id,name,projectName,builds($locator(running:any,canceled:false,count:1,branch:default:any),build(number,status,state,statusText,branchName,lastChanges(change(username,comment)))))'

	return request.get(`https://${config.host}/httpAuth/app/rest/${query}`)
		.set('Accept', 'application/json')
		.set('Authorization', authorization)
		.then(function (response) {

			const buildType = response.body.buildType;
			res.json(buildType);
			
		}).catch(next)
})

app.get('/', function(req, res) {
	res.redirect('/dash')
})
app.use('/dash', router)

app.listen(8888, () => console.info('Server is on port :8888'));
